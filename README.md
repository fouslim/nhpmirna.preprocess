# P162+VB013+P181 miRNA: preprocessing source code #

### preprocessing:
code:  
- R code: [[PDF]](20171214_NHPmiRNA.fixedEffect.pdf)  

input:  
- raw gene count matrix: [[CSV]](https://storage.googleapis.com/nhpmirna_20170505/preprocessing/gene_counts.csv)  
- samples annotation: [[CSV]](https://storage.googleapis.com/nhpmirna_20170505/preprocessing/nhpmirna.metadata.csv)  
- alignment quality metrics: [[TSV]](https://storage.googleapis.com/nhpmirna_20170505/preprocessing/nphmirna.readstats.tsv)  

output:  
- non-normalized SeqExpressionSet: [[RDA]](https://storage.googleapis.com/nhpmirna_20170505/preprocessing/nhpmirna.esetRaw.RData)  
- TMM-normalized SeqExpressionSet: [[RDA]](https://storage.googleapis.com/nhpmirna_20170505/preprocessing/nhpmirna.eset.RData)  